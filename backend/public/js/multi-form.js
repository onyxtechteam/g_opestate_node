
//jQuery time
let current_fs, next_fs, previous_fs; //fieldsets
let left, opacity, scale; //fieldset properties which we will animate
let animating; //flag to prevent quick multi-click glitches

$(".next").click(function () {
  if (animating) return false;
  animating = true;

  current_fs = $(this).parent();
  next_fs = $(this).parent().next();

  //activate next step on progressbar using the index of next_fs
  $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

  //show the next fieldset
  next_fs.show();
  //hide the current fieldset with style
  current_fs.animate({ opacity: 0 }, {
    step: function (now, mx) {
      //as the opacity of current_fs reduces to 0 - stored in "now"
      //1. scale current_fs down to 80%
      scale = 1 - (1 - now) * 0.2;
      //2. bring next_fs from the right(50%)
      left = (now * 50) + "%";
      //3. increase opacity of next_fs to 1 as it moves in
      opacity = 1 - now;
      current_fs.css({
        'transform': 'scale(' + scale + ')',
        'position': 'absolute'
      });
      next_fs.css({ 'left': left, 'opacity': opacity });
    },
    duration: 800,
    complete: function () {
      current_fs.hide();
      animating = false;
    },
    //this comes from the custom easing plugin
    easing: 'easeInOutBack'
  });
});

$(".previous").click(function () {
  if (animating) return false;
  animating = true;

  current_fs = $(this).parent();
  previous_fs = $(this).parent().prev();

  //de-activate current step on progressbar
  $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

  //show the previous fieldset
  previous_fs.show();
  //hide the current fieldset with style
  current_fs.animate({ opacity: 0 }, {
    step: function (now, mx) {
      //as the opacity of current_fs reduces to 0 - stored in "now"
      //1. scale previous_fs from 80% to 100%
      scale = 0.8 + (1 - now) * 0.2;
      //2. take current_fs to the right(50%) - from 0%
      left = ((1 - now) * 50) + "%";
      //3. increase opacity of previous_fs to 1 as it moves in
      opacity = 1 - now;
      current_fs.css({ 'left': left });
      previous_fs.css({ 'transform': 'scale(' + scale + ')', 'opacity': opacity });
    },
    duration: 800,
    complete: function () {
      current_fs.hide();
      animating = false;
    },
    //this comes from the custom easing plugin
    easing: 'easeInOutBack'
  });
});

$('.create_listing').submit(function (event) {
  event.preventDefault();

  const form = $(this);

  $.ajax({
    url: '/admin/listings/create',
    data: form.serialize(),
    processData: false,
    type: 'POST',
    beforeSend: () => {
      $('.submit').text('Loadinig')
    },
    success: function (data) {
      $('.col-md-6').prepend(`<div class="alert alert-success alert-dismissible fade show" role="alert">
      <strong>${data.msg}
      </strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>`)
      form.reset()
      $('.submit').tex('Submit')

    }, error: function (error) {
      $('.col-md-6').prepend(`<div class="alert alert-warning alert-dismissible fade show" role="alert">
      <strong>${error.msg}
      </strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>`)
    }
  });

});

// message request form
$('.edit_listing').submit(function (event) {
  event.preventDefault();

  const form = $(this);
  const formData = form.serializeArray()
  let _id = 0
  formData.forEach(field => {
    if (field.name == 'estate_id') {
      _id = field.value
    }
  });

  $.ajax({
    url: `/admin/listings/${_id}/update`,
    data: form.serialize(),
    processData: false,
    type: 'PUT',
    beforeSend: () => {
      $('.submit').text('Loading')
    },
    success: function (data) {
      $('.col-md-6').prepend(`<div class="alert alert-success alert-dismissible fade show" role="alert">
      <strong>${data.msg}
      </strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>`)
      form.reset()
      $('.submit').tex('Update')
    }, error: function (error) {
      $('.col-md-6').prepend(`<div class="alert alert-warning alert-dismissible fade show" role="alert">
      <strong>${error.msg}
      </strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>`)
    }
  });

});

$(document).ready(function () {
  let maxField = 5;
  let addButton = $('.add_button');
  let wrapper = $('.field_wrapper');
  let fieldHTML = `<div class="form-group row">
                  <div div class="col-md-11" >
                    <input type="text" name="features[]" value="" required class="form-control"
                      placeholder="Estate Feature" />
                  </div>
                  <div div class="col-md-1" style="align-self: center;">
                  <a href="javascript:void(0);" class="remove_button" title="Add field"><i
                    class="pe-7s-close"></i></a>
                  </div>
                </div>`;
  let x = 1;

  $(addButton).click(function () {
    if (x < maxField) {
      x++;
      $(wrapper).append(fieldHTML);
    }
  });

  //Once remove button is clicked
  $(wrapper).on('click', '.remove_button', function (e) {
    e.preventDefault();
    $(this).closest('.row').remove();
    x--;
  });
});