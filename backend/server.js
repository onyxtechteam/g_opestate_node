import express from "express"
import cors from "cors"
import expressEjsLayouts from "express-ejs-layouts"
import session from 'express-session'
import bodyParser from 'body-parser'
import flash from 'express-flash'
import publicRoute from "./routes/public.js"
import adminRouter from "./routes/admin.js"
import cookieParser from "cookie-parser"
import GuestPageController from "./controller/GuestPages.js"
import AdminController from "./controller/AdminController.js"

const app = express()

const middleWares = [
  express.static('./public'),
  cors(),
  express.json(),
  session({
    secret: 'super-secret-key',
    key: 'super-secret-cookie',
    resave: false,
    saveUninitialized: false,
    cookie: { maxAge: 6000000, sameSite: true },
    name: 'SESSION_NAME'
  }),
  flash(),
  bodyParser.urlencoded({ extended: true }),
  cookieParser()
]

app.use(middleWares)
app.use(expressEjsLayouts)

// templating
app.set('layout', './guest/layout')
app.set('views', './views/pages');
app.set('view engine', 'ejs');

// routes

// auth middleWares

const redirectHome = (req, res, next) => {
  if (req.session.token) {
    res.redirect('/admin/');
  } else {
    next();
  }
};

const requireAuth = (req, res, next) => {
  if (req?.session?.token) {
    next()
  } else {
    res.redirect('/login');
  }
};

app.use("/", publicRoute)
app.use("/admin", adminRouter)
app.get("/login", redirectHome, (req, res) => res.render('./admin/login', { layout: false, message: {} }))
app.post("/admin/login", AdminController.login)

app.use("*", GuestPageController.error404)

export default app