import mongoose from "mongoose"

const TeamSchema = new mongoose.Schema(
  {
    fullname: String,
    title: String,
    summary: String,
    avatar: String,
    _id: Number
  },
  { collection: 'teams' }
)

const TeamModel = mongoose.model('Teams', TeamSchema)
export default TeamModel