import mongoose from "mongoose"

const ClientMessageSchema = new mongoose.Schema(
  {
    name: String,
    email: String,
    message: String,
    _id: Number
  },
  { collection: 'client_messages' }
)

const ClientMessageModel = mongoose.model('ClientMessage', ClientMessageSchema)
export default ClientMessageModel