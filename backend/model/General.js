import mongoose from "mongoose"

const GeneralSchema = new mongoose.Schema(
  {

    ceo: String,
    experience: Number,
    client_no: Number,
    _id: Number,
    project_no: Number,
    expert_no: Number
  },
  { collection: 'generals' }
)

const GeneralModel = mongoose.model('Generals', GeneralSchema)
export default GeneralModel