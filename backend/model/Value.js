import mongoose from "mongoose"

const ValueSchema = new mongoose.Schema(
  {
    value: String,
    title:String,
    _id: Number
  },
  { collection: 'values' }
)

const ValueModel = mongoose.model('Value', ValueSchema)
export default ValueModel