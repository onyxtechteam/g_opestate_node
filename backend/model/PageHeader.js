import mongoose from "mongoose"

const PageHeaderSchema = new mongoose.Schema(
  {
    content: String,
    page:String,
    _id: Number
  },
  { collection: 'page_headers' }
)

const PageHeaderModel = mongoose.model('PageHeader', PageHeaderSchema)
export default PageHeaderModel