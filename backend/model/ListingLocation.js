import mongoose from "mongoose"

const ListingLocationSchema = new mongoose.Schema(
  {
    location: String,
    summary: String,
    images: [String],
    videos: [String],
    features: [String],
    socials: { type: Map, of: String },
    title: String,
    slug: String,
    _id: Number,
    estate_gps: Object,
    allocated: [Object],
  },
  { collection: 'listing_locations' }
)

const ListingLocationModel = mongoose.model('ListingLocation', ListingLocationSchema)
export default ListingLocationModel