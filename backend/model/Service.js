import mongoose from "mongoose"

const ServicesSchema = new mongoose.Schema(
  {
    service: String,
    summary: String,
    _id: Number
  },
  { collection: 'services' }
)

const ServiceModel = mongoose.model('Service', ServicesSchema)
export default ServiceModel