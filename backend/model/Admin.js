import mongoose from "mongoose"

const AdminSchema = new mongoose.Schema(
  {

    fullname: String,
    phone: String,
    email: String,
    _id: Number,
    username: String,
    password: String
  },
  { collection: 'admins' }
)

const AdminModel = mongoose.model('Admins', AdminSchema)
export default AdminModel