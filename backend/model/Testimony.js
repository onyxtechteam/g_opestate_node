import mongoose from "mongoose"

const TestimoniesSchema = new mongoose.Schema(
  {
    testimony: String,
    fullname: String,
    avatar: String,
    profession: String,
    _id: Number
  },
  { collection: 'client_testimonies' }
)

const TestimonyModel = mongoose.model('ClientTestimonies', TestimoniesSchema)
export default TestimonyModel