import mongoose from "mongoose"

const SliderSchema = new mongoose.Schema(
  {
    caption: String,
    title: String,
    no: Number,
    img_url: String,
    _id: Number
  },
  { collection: 'sliders' }
)

const SliderModel = mongoose.model('Sliders', SliderSchema)
export default SliderModel