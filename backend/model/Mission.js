import mongoose from "mongoose"

const MissionSchema = new mongoose.Schema(
  {
    mission: String,
    title:String,
    _id: Number
  },
  { collection: 'missions' }
)

const MissionModel = mongoose.model('Mission', MissionSchema)
export default MissionModel