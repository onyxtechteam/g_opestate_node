import mongoose from "mongoose"

const FaqSchema = new mongoose.Schema(
  {
    answer: String,
    question: String,
    _id: Number
  },
  { collection: 'faqs' }
)

const FaqModel = mongoose.model('Faq', FaqSchema)
export default FaqModel