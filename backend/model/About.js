import mongoose from "mongoose"

const AboutSchema = new mongoose.Schema(
  {
    about_us: String,
    _id: Number
  },
  { collection: 'about_us' }
)

const AboutModel = mongoose.model('AboutUs', AboutSchema)
export default AboutModel