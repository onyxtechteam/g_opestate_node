import mongoose from "mongoose"

const ClientBioDataSchema = new mongoose.Schema(
  {
    estate_id: String,
    fullname: String,
    message: String,
    phone: String,
    email: String,
    _id: Number
  },
  { collection: 'client_diodatas' }
)

const ClientBioDataModel = mongoose.model('ClientBioData', ClientBioDataSchema)
export default ClientBioDataModel