import mongoose from "mongoose"

const PropertiesSchema = new mongoose.Schema(
  {
    title: String,
    location: String,
    description: String,
    images: [],
    _id: Number
  },
  { collection: 'properties' }
)

const PropertyModel = mongoose.model('Property', PropertiesSchema)
export default PropertyModel