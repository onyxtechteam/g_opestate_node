import mongoose from "mongoose"

const FeaturedImagesSchema = new mongoose.Schema(
  {
    image: String,
    caption:String,
    _id: Number
  },
  { collection: 'featured_images' }
)

const FeaturedImagesModel = mongoose.model('FeaturedImages', FeaturedImagesSchema)
export default FeaturedImagesModel