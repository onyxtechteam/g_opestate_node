import { createHash, randomBytes } from 'crypto';

const getHashedPassword = (password) => {
  const sha256 = createHash('sha256');
  const hash = sha256.update(password).digest('base64');
  return hash;
}

const generateAuthToken = () => {
  return randomBytes(30).toString('hex');
}

export { getHashedPassword, generateAuthToken }