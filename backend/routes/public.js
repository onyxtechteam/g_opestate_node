import { Router } from 'express'
import ClientMessageController from '../controller/ClientMessage.js';
import GuestPageController from '../controller/GuestPages.js';

const publicRoute = Router()

publicRoute.route("/").get(GuestPageController.homePage)
publicRoute.route("/services").get(GuestPageController.servicePage)
publicRoute.route("/faq").get(GuestPageController.faqPage)
publicRoute.route("/about").get(GuestPageController.aboutPage)
publicRoute.route("/contact_us").get(GuestPageController.contactUsPage)
publicRoute.route("/listings").get(GuestPageController.listingPage)
publicRoute.route("/listings/:id/details").get(GuestPageController.listingDetailsPage)
publicRoute.route("/contact_us").post(ClientMessageController.store)
publicRoute.route("/:id/home").get(GuestPageController.subEstateHome)
publicRoute.route("/:id/media").get(GuestPageController.subEstateMedia)

export default publicRoute