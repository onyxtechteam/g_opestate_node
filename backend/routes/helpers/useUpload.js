import multer from 'multer'
import path from 'path'

const slideStorage = (field = null, group) => {
  return multer.diskStorage(
    {
      destination: './public/uploads/',
      filename: function (req, file, cb) {
        if (file.fieldname === 'images' || "videos") {
          return cb(null, `${group}_${Math.floor(Math.random() * 100)}${path.extname(file.originalname)}`);
        } else {
          const upload_id = req.body[field]
          return cb(null, `${group}_${upload_id}${path.extname(file.originalname)}`);
        }
      }
    }
  )
}

const imageUpload = (field, group) => (
  multer({
    limits: {
      fileSize: 10000000,
    },
    storage: slideStorage(field, group),
    dest: './public/uploads/',
    fileFilter(req, file, cb) {
      if (!file.originalname.match(/\.(png|jpg)$/)) {
        return cb('Please upload an Image')
      }
      if (req?.files?.length > 6) {
        return cb('Sorry You can select more than 6 images')
      }
      cb(undefined, true)
    }
  })
)

const videoStorage = (field = null, group) => {
  return multer.diskStorage(
    {
      destination: './public/uploads/videos',
      filename: function (req, file, cb) {
        if (file.fieldname === "videos") {
          return cb(null, `${group}_${Math.floor(Math.random() * 100)}${path.extname(file.originalname)}`);
        } else {
          const upload_id = req.body[field]
          return cb(null, `${group}_${upload_id}${path.extname(file.originalname)}`);
        }
      }
    }
  )
}



const videoUpload = (field, group) => (
  multer({
    limits: {
      fileSize: 5000000000,
    },
    storage: videoStorage(field, group),
    dest: './public/uploads/videos/',
    fileFilter(req, file, cb) {
      if (req?.files?.length > 6) {
        return cb('Sorry You can select more than 6 Videos')
      }
      cb(undefined, true)
    }
  })
)

export { imageUpload, videoUpload }