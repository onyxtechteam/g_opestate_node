import { Router } from 'express'
import SliderController from '../controller/Slider.js'
import TeamController from '../controller/Team.js';
import methodOverride from 'method-override'
import { imageUpload, videoUpload } from './helpers/useUpload.js';
import AboutController from '../controller/About.js';
import TestimonyController from '../controller/Testimony.js';
import FaqController from '../controller/Faq.js';
import ServiceController from '../controller/Service.js';
import MissionController from '../controller/Mission.js';
import PropertyController from '../controller/Property.js';
import ValueController from '../controller/Value.js';
import ListingLocationController from '../controller/ListingLocation.js';
import FeaturedImagesController from '../controller/FeaturedImages.js';
import ClientMessageController from '../controller/ClientMessage.js';
import ClientBioDataController from '../controller/ClientBiodata.js';
import GeneralController from '../controller/General.js';
import AdminController from '../controller/AdminController.js';

// import PageHeaderController from '../controller/PageHeader.js';

const adminRouter = Router()

const requireAuth = (req, res, next) => {
  if (req?.session?.token) {
    next()
  } else {
    res.redirect('/login');
  }
};
adminRouter.use(methodOverride("_method", {
  methods: ["POST", "GET"]
}));


// slider route
adminRouter.route("/sliders/").get(requireAuth,SliderController.index)
adminRouter.route("/sliders/create").get(requireAuth,(req, res) => res.render('./admin/sliders/create', { layout: './admin/layout', message: {} }))
adminRouter.route("/sliders/create").post(requireAuth,imageUpload('no', 'slide').single('imgUrl'), SliderController.store)
adminRouter.route("/sliders/:id/edit").get(requireAuth,SliderController.edit)
adminRouter.route("/sliders/:id/update").put(requireAuth,imageUpload('no', 'slide').single('imgUrl'), SliderController.update)
adminRouter.route("/sliders/:id/delete").delete(requireAuth,SliderController.delete)

// teams route
adminRouter.route("/teams/").get(requireAuth,TeamController.index)
adminRouter.route("/teams/create").get(requireAuth,(req, res) => res.render('./admin/teams/create', { layout: './admin/layout', message: {} }))
adminRouter.route("/teams/create").post(requireAuth,imageUpload('fullname', 'team').single('avatar'), TeamController.store)
adminRouter.route("/teams/:id/edit").get(requireAuth,TeamController.edit)
adminRouter.route("/teams/:id/update").put(requireAuth,imageUpload('fullname', 'team').single('avatar'), TeamController.update)
adminRouter.route("/teams/:id/delete").delete(requireAuth,TeamController.delete)

// about route
adminRouter.route("/abouts/").get(requireAuth,AboutController.index)
adminRouter.route("/abouts/create").get(requireAuth,(req, res) => res.render('./admin/abouts/create', { layout: './admin/layout', message: {} }))
adminRouter.route("/abouts/create").post(requireAuth,AboutController.store)
adminRouter.route("/abouts/:id/edit").get(requireAuth,AboutController.edit)
adminRouter.route("/abouts/:id/update").put(requireAuth,AboutController.update)
adminRouter.route("/abouts/:id/delete").delete(requireAuth,AboutController.delete)

// testimonies route
adminRouter.route("/testimonies/").get(requireAuth,TestimonyController.index)
adminRouter.route("/testimonies/create").get(requireAuth,(req, res) => res.render('./admin/testimonies/create', { layout: './admin/layout', message: {} }))
adminRouter.route("/testimonies/create").post(requireAuth,imageUpload('fullname', 'client').single('avatar'), TestimonyController.store)
adminRouter.route("/testimonies/:id/edit").get(requireAuth,TestimonyController.edit)
adminRouter.route("/testimonies/:id/update").put(requireAuth,imageUpload('fullname', 'client').single('avatar'), TestimonyController.update)
adminRouter.route("/testimonies/:id/delete").delete(requireAuth,TestimonyController.delete)

// faqs route
adminRouter.route("/faqs/").get(requireAuth,FaqController.index)
adminRouter.route("/faqs/create").get(requireAuth,(req, res) => res.render('./admin/faqs/create', { layout: './admin/layout', message: {} }))
adminRouter.route("/faqs/create").post(requireAuth,FaqController.store)
adminRouter.route("/faqs/:id/edit").get(requireAuth,FaqController.edit)
adminRouter.route("/faqs/:id/update").put(requireAuth,FaqController.update)
adminRouter.route("/faqs/:id/delete").delete(requireAuth,FaqController.delete)


// service route
adminRouter.route("/services/").get(requireAuth,ServiceController.index)
adminRouter.route("/services/create").get(requireAuth,(req, res) => res.render('./admin/services/create', { layout: './admin/layout', message: {} }))
adminRouter.route("/services/create").post(requireAuth,ServiceController.store)
adminRouter.route("/services/:id/edit").get(requireAuth,ServiceController.edit)
adminRouter.route("/services/:id/update").put(requireAuth,ServiceController.update)
adminRouter.route("/services/:id/delete").delete(requireAuth,ServiceController.delete)

// mission route
adminRouter.route("/abouts/missions/").get(requireAuth,MissionController.index)
adminRouter.route("/abouts/missions/create").get(requireAuth,(req, res) => res.render('./admin/missions/create', { layout: './admin/layout', message: {} }))
adminRouter.route("/abouts/missions/create").post(requireAuth,MissionController.store)
adminRouter.route("/abouts/missions/:id/edit").get(requireAuth,MissionController.edit)
adminRouter.route("/abouts/missions/:id/update").put(requireAuth,MissionController.update)
adminRouter.route("/abouts/missions/:id/delete").delete(requireAuth,MissionController.delete)

// mission route
adminRouter.route("/abouts/values/").get(requireAuth,ValueController.index)
adminRouter.route("/abouts/values/create").get(requireAuth,(req, res) => res.render('./admin/values/create', { layout: './admin/layout', message: {} }))
adminRouter.route("/abouts/values/create").post(requireAuth,ValueController.store)
adminRouter.route("/abouts/values/:id/edit").get(requireAuth,ValueController.edit)
adminRouter.route("/abouts/values/:id/update").put(requireAuth,ValueController.update)
adminRouter.route("/abouts/values/:id/delete").delete(requireAuth,ValueController.delete)

// // page_header route
// adminRouter.route("/settings/page_headers/").get(requireAuth,PageHeaderController.index)
// adminRouter.route("/settings/page_headers/create").get(requireAuth,(req, res) => res.render('./admin/page_headers/create', { layout: './admin/layout', message: {} }))
// adminRouter.route("/settings/page_headers/create").post(requireAuth,PageHeaderController.store)
// adminRouter.route("/settings/page_headers/:id/edit").get(requireAuth,PageHeaderController.edit)
// adminRouter.route("/settings/page_headers/:id/update").put(requireAuth,PageHeaderController.update)
// adminRouter.route("/settings/page_headers/:id/delete").delete(requireAuth,PageHeaderController.delete)

//  properties route
adminRouter.route("/properties/").get(requireAuth,PropertyController.index)
adminRouter.route("/properties/create").get(requireAuth,(req, res) => res.render('./admin/properties/create', { layout: './admin/layout', message: {} }))
adminRouter.route("/properties/create").post(requireAuth,imageUpload('title', 'properties').array('images',), PropertyController.store)
adminRouter.route("/properties/:id/edit").get(requireAuth,PropertyController.edit)
adminRouter.route("/properties/:id/update").put(requireAuth,imageUpload('title', 'properties').array('images',), PropertyController.update)
adminRouter.route("/properties/:id/delete").delete(requireAuth,PropertyController.delete)

//  locations route
adminRouter.route("/listings/").get(requireAuth,ListingLocationController.index)
adminRouter.route("/listings/create").get(requireAuth,(req, res) => res.render('./admin/listing_location/create', { layout: './admin/layout' }))
adminRouter.route("/listings/:id/upload_layout").get(requireAuth,ListingLocationController.renderUploadLayout)
adminRouter.route("/listings/:id/upload_layout").put(requireAuth,ListingLocationController.uploadLayout)
adminRouter.route("/listings/:id/load_layout").get(requireAuth,ListingLocationController.fetchLayout)
adminRouter.route("/listings/:id/upload_images").get(requireAuth,ListingLocationController.renderUploadImage)
adminRouter.route("/listings/:id/upload_images").put(requireAuth,imageUpload(null, 'estate').array('images',), ListingLocationController.imagesUpload)
adminRouter.route("/listings/:id/upload_videos").get(requireAuth,ListingLocationController.renderUploadVideo)
adminRouter.route("/listings/:id/upload_videos").put(requireAuth,videoUpload(null, 'estate').array('videos',), ListingLocationController.videosUpload)
adminRouter.route("/listings/create").post(requireAuth,ListingLocationController.store)
adminRouter.route("/listings/:id/edit").get(requireAuth,ListingLocationController.edit)
adminRouter.route("/listings/:id/show").get(requireAuth,ListingLocationController.show)
adminRouter.route("/listings/:id/update").put(requireAuth,ListingLocationController.update)
adminRouter.route("/listings/:estate_id/delete_video").post(requireAuth,ListingLocationController.videosDelete)
adminRouter.route("/listings/:estate_id/delete_image").post(requireAuth,ListingLocationController.imagesDelete)
adminRouter.route("/listings/:id/delete").delete(requireAuth,ListingLocationController.delete)

//  featured_images route
adminRouter.route("/featured_images/").get(requireAuth,FeaturedImagesController.index)
adminRouter.route("/featured_images/create").get(requireAuth,(req, res) => res.render('./admin/featured_images/create', { layout: './admin/layout', message: {} }))
adminRouter.route("/featured_images/create").post(requireAuth,imageUpload('caption', 'featured_images').single('featured_image',), FeaturedImagesController.store)
adminRouter.route("/featured_images/:id/edit").get(requireAuth,FeaturedImagesController.edit)
adminRouter.route("/featured_images/:id/update").put(requireAuth,imageUpload('caption', 'featured_images').single('featured_image',), FeaturedImagesController.update)
adminRouter.route("/featured_images/:id/delete").delete(requireAuth,FeaturedImagesController.delete)

//  admin users route
adminRouter.route("/admins/").get(requireAuth,AdminController.index)
adminRouter.route("/admins/create").get(requireAuth,(req, res) => res.render('./admin/admins/create', { layout: './admin/layout', message: {} }))
adminRouter.route("/admins/create").post(requireAuth,AdminController.store)
// adminRouter.route("/admins/:id/edit").get(requireAuth,AdminController.edit)
// adminRouter.route("/admins/:id/update").put(requireAuth,imageUpload('caption', 'admins').single('featured_image',), AdminController.update)
adminRouter.route("/admins/:id/delete").delete(requireAuth,AdminController.delete)
adminRouter.route("/logout").post(requireAuth,AdminController.logout)

//  site general settings route
adminRouter.route("/generals/").get(requireAuth,GeneralController.index)
adminRouter.route("/generals/create").get(requireAuth,(req, res) => res.render('./admin/generals/create', { layout: './admin/layout', message: {} }))
adminRouter.route("/generals/create").post(requireAuth,GeneralController.store)
adminRouter.route("/generals/:id/edit").get(requireAuth,GeneralController.edit)
adminRouter.route("/generals/:id/update").put(requireAuth,GeneralController.update)
adminRouter.route("/generals/:id/delete").delete(requireAuth,GeneralController.delete)

//  client_messages
adminRouter.route("/client_messages/").get(requireAuth,ClientMessageController.index)
adminRouter.route("/client_messages/store").post(requireAuth,ClientMessageController.store)
adminRouter.route("/client_messages/:id/delete").delete(requireAuth,ClientMessageController.delete)

//  client_messages_estate request
adminRouter.route("/clients_request/").get(requireAuth,ClientBioDataController.index)
adminRouter.route("/clients_request/store").post(requireAuth,ClientBioDataController.store)
adminRouter.route("/clients_request/:id/delete").delete(requireAuth,ClientBioDataController.delete)

// 
adminRouter.route("/").get(requireAuth,(req, res) => res.render('./admin/index', { layout: './admin/layout' }))
// adminRouter.route("/").get(requireAuth,requireAuth, (req, res) => res.render('./admin/index', { layout: './admin/layout' }))

export default adminRouter