import ClientBioDataModel from "../model/ClientBiodata.js"
import EstateModel from "../model/ListingLocation.js"
import { check, validationResult, matchedData } from 'express-validator'

export default class ClientBioDataController {

  static async index(req, res) {
    try {
      const result = await ClientBioDataModel.find()

      const clients_biodata = await Promise.all(
        result.map(async (element) => {
          const estate = await EstateModel.findById(element._id);
          const { title } = estate
          return {
            ...element._doc,
            estate: title,
          };
        })
      );
      return res.render('./admin/clients_request/index', { layout: './admin/layout', data: clients_biodata })
    } catch (error) {
      console.log(error)
      res.status(500).json({ error })
    }
  }


  static async store(req, res) {
    try {
      const { fullname, phone, email, message, estate_id } = req.body
      const last_entry = await ClientBioDataModel.findOne().sort({ field: 'asc', _id: -1 }).limit(1)
      const count = last_entry?._id ? last_entry?._id + 1 : 1
      await ClientBioDataModel.create({ fullname, phone, email, message, estate_id, _id: count })
      return res.json({ type: 'success', msg: 'Message Request Sent' });
    } catch (error) {
      return res.flash({ type: 'error', msg: 'Something went wrong' });
    }

  }


  static async delete(req, res) {
    try {

      await ClientBioDataModel.findByIdAndRemove(req.params.id)
      return res.redirect('/admin/clients_request/')
    } catch (error) {
      res.status(500).json({ error })
    }

  }

}