import GeneralModel from "../model/General.js"
import { check, validationResult, matchedData } from 'express-validator'

export default class GeneralController {

  static async index(req, res) {
    try {
      const result = await GeneralModel.find().limit(1)

      return res.render('./admin/generals/index', { layout: './admin/layout', data: result })
    } catch (error) {
      console.log(error)
      return req.flash('error', 'Reload pLease')
    }
  }


  static async store(req, res) {
    try {
      const { ceo, project_no, client_no, experience, expert_no } = req.body
      console.log(req.body)
      const last_entry = await GeneralModel.findOne().sort({ field: 'asc', _id: -1 }).limit(1)
      const count = last_entry?._id ? last_entry?._id + 1 : 1
      await GeneralModel.create({ ceo, project_no, client_no, experience, expert_no, _id: count })

      res.render('./admin/generals/create', { layout: './admin/layout', message: { type: 'success', msg: 'Item Added' } })

    } catch (error) {
      res.render('./admin/generals/create', { layout: './admin/layout', message: { type: 'error', msg: 'Something went wrong' } })
    }

  }

  static async edit(req, res) {
    try {

      const result = await GeneralModel.findById(req.params.id)
      return res.render('./admin/generals/edit', { layout: './admin/layout', message: {}, data: result })
    } catch (error) {
      res.status(500).json({ error })
    }

  }

  static async update(req, res,) {
    try {
      const { ceo, project_no, client_no, experience, expert_no } = req.body
      const body = {
        ceo, project_no, client_no, experience, expert_no
      }

      await GeneralModel.findByIdAndUpdate(req.params.id, { $set: body })
      return res.redirect('/admin/generals/')
    } catch (error) {
      const result = await GeneralModel.findById(req.params.id)
      return res.render('./admin/generals/edit', { layout: './admin/layout', message: { type: 'error', msg: 'Something went wrong' }, data: result })
    }

  }

  static async delete(req, res) {
    try {

      await GeneralModel.findByIdAndRemove(req.params.id)
      return res.redirect('/admin/generals/')
    } catch (error) {
      res.status(500).json({ error })
    }

  }
}