import { check, validationResult, matchedData } from 'express-validator'
import MissionModel from '../model/Mission.js'

export default class MissionController {

  static async index(req, res) {
    try {
      const result = await MissionModel.find().limit(2)
      return res.render('./admin/missions/index', { layout: './admin/layout', data: result })
    } catch (error) {
      res.status(500).json({ error })
    }

  }


  static async store(req, res) {
    try {

      const { mission,title } = req.body
      const last_entry = await MissionModel.findOne().sort({ field: 'asc', _id: -1 }).limit(1)
      const count = last_entry?._id ? last_entry?._id + 1 : 1
      await MissionModel.create({ mission, title,_id: count})
      res.render('./admin/missions/create', { layout: './admin/layout', message: { type: 'success', msg: 'Mission Statement Added' } })

    } catch (error) {
      console.log(error)
      res.render('./admin/missions/create', { layout: './admin/layout', message: { type: 'error', msg: 'Something went wrong' } })
    }

  }

  static async edit(req, res) {
    try {

      const result = await MissionModel.findById(req.params.id)
      return res.render('./admin/missions/edit', { layout: './admin/layout', message: { }, data: result })
    } catch (error) {
      res.status(500).json({ error })
    }

  }

  static async update(req, res, next) {
    try {
      const { mission,title } = req.body

      const body = { mission,title }

      await MissionModel.findByIdAndUpdate(req.params.id, { $set: body })
      req.flash('info','Updated')
      return res.redirect('/admin/abouts/missions/')
    } catch (error) {
      console.log(error)
      const result = await MissionModel.findById(req.params.id)
      return res.render('./admin/missions/edit', { layout: './admin/layout', message: { type: 'error', msg: 'Something went wrong' }, data: result })
    }

  }

  static async delete(req, res) {
    try {

      const result = await MissionModel.findByIdAndRemove(req.params.id)
      req.flash('info','Deleted')
      return res.redirect('/admin/abouts/missions/')
    } catch (error) {
      res.status(500).json({ error })
    }

  }

}