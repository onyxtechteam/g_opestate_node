import PropertyModel from "../model/Property.js"
import { check, validationResult, matchedData } from 'express-validator'
import ListingLocationModel from "../model/ListingLocation.js"

export default class PropertyController {

  static async index(req, res) {
    try {
      const result = await PropertyModel.find().limit(5)
      const listings = await ListingLocationModel.find().count()
      return res.render('./admin/properties/index', { layout: './admin/layout', data: result,count:{listings} })
    } catch (error) {
      res.status(500).json({ error })
    }

  }


  static async store(req, res) {
    try {

      const { title, location, description, } = req.body

      const last_entry = await PropertyModel.findOne().sort({ field: 'asc', _id: -1 }).limit(1)
      const count = last_entry?._id ? last_entry?._id + 1 : 1
      let images = []
      const files= req.files
      files.forEach((file) => {
        images.push(file.filename)
      });
      await PropertyModel.create({ title, location, description, _id: count, images })
      res.render('./admin/properties/create', { layout: './admin/layout', message: { type: 'success', msg: 'Property Added' } })

    } catch (error) {
      console.log(error)
      res.render('./admin/properties/create', { layout: './admin/layout', message: { type: 'error', msg: 'Something went wrong' } })
    }

  }

  static async edit(req, res) {
    try {

      const result = await PropertyModel.findById(req.params.id)
      return res.render('./admin/properties/edit', { layout: './admin/layout', message: { }, data: result })
    } catch (error) {
      res.status(500).json({ error })
    }

  }

  static async update(req, res) {
    try {
      const {title, location, description, } = req.body
      const body = req?.file?.filename ? {
        title, location, description,
        images: req.file.filename,
      } : {
       title, location, description,
      }

      await PropertyModel.findByIdAndUpdate(req.params.id, { $set: body })
      req.flash('info','updated')
      return res.redirect('/admin/properties/')
    } catch (error) {
      return req.flash('error','something went wrong')
    }

  }

  static async delete(req, res) {
    try {

      const result = await PropertyModel.findByIdAndRemove(req.params.id)
      req.flash('info','deleted')
      return res.redirect('/admin/properties/')
    } catch (error) {
      req.flash('error','something went wrong')
    }

  }

}