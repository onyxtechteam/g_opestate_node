import FeaturedImagesModel from "../model/FeaturedImages.js"
import { check, validationResult, matchedData } from 'express-validator'

export default class FeaturedImagesController {

  static async index(req, res) {
    try {
      const result = await FeaturedImagesModel.find()
      return res.render('./admin/featured_images/index', { layout: './admin/layout', data: result })
    } catch (error) {
      res.status(500).json({ error })
    }

  }


  static async store(req, res) {
    try {

      const {caption } = req.body
      const last_entry = await FeaturedImagesModel.findOne().sort({field: 'asc', _id: -1 }).limit(1)
      const count = last_entry?._id ? last_entry?._id +1 : 1
      await FeaturedImagesModel.create({_id: count , image: req.file.filename, caption })
      res.render('./admin/featured_images/create', { layout: './admin/layout', message: { type: 'success', msg: 'Image Added' } })

    } catch (error) {
      console.log(error)
      res.render('./admin/featured_images/create', { layout: './admin/layout', message: { type: 'error', msg: 'Something went wrong' } })
    }

  }

  static async edit(req, res) {
    try {

      const result = await FeaturedImagesModel.findById(req.params.id)
      return res.render('./admin/featured_images/edit', { layout: './admin/layout', message: { }, data: result })
    } catch (error) {
      res.status(500).json({ error })
    }

  }

  static async update(req, res,) {
    try {
      const { caption } = req.body

      const body = req.file?.filename ? {
        caption,
        image: req.file.filename,
      } : {
        caption,
      }

      const result = await FeaturedImagesModel.findByIdAndUpdate(req.params.id, { $set: body })
      return res.redirect('/admin/featured_images/')
    } catch (error) {
      const result = await FeaturedImagesModel.findById(req.params.id)
      return res.render('./admin/featured_images/edit', { layout: './admin/layout', message: { type: 'error', msg: 'Something went wrong' }, data: result })
    }

  }

  static async delete(req, res) {
    try {

      const result = await FeaturedImagesModel.findByIdAndRemove(req.params.id)
      console.log(result)
      return res.redirect('/admin/featured_images/')
    } catch (error) {
      res.status(500).json({ error })
    }

  }

}