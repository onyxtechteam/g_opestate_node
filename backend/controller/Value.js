import { check, validationResult, matchedData } from 'express-validator'
import ValueModel from '../model/Value.js'

export default class ValueController {

  static async index(req, res) {
    try {
      const result = await ValueModel.find().limit(2)
      return res.render('./admin/values/index', { layout: './admin/layout', data: result })
    } catch (error) {
      res.status(500).json({ error })
    }

  }


  static async store(req, res) {
    try {

      const { value,title } = req.body
      const last_entry = await ValueModel.findOne().sort({ field: 'asc', _id: -1 }).limit(1)
      const count = last_entry?._id ? last_entry?._id + 1 : 1
      await ValueModel.create({ value, title,_id: count})
      res.render('./admin/values/create', { layout: './admin/layout', message: { type: 'success', msg: 'Value Statement Added' } })

    } catch (error) {
      console.log(error)
      res.render('./admin/values/create', { layout: './admin/layout', message: { type: 'error', msg: 'Something went wrong' } })
    }

  }

  static async edit(req, res) {
    try {

      const result = await ValueModel.findById(req.params.id)
      return res.render('./admin/values/edit', { layout: './admin/layout', message: { }, data: result })
    } catch (error) {
      res.status(500).json({ error })
    }

  }

  static async update(req, res, next) {
    try {
      const { value,title } = req.body

      const body = { value,title }

      await ValueModel.findByIdAndUpdate(req.params.id, { $set: body })
      req.flash('info','Updated')
      return res.redirect('/admin/abouts/values/')
    } catch (error) {
      console.log(error)
      const result = await ValueModel.findById(req.params.id)
      return res.render('./admin/values/edit', { layout: './admin/layout', message: { type: 'error', msg: 'Something went wrong' }, data: result })
    }

  }

  static async delete(req, res) {
    try {

      const result = await ValueModel.findByIdAndRemove(req.params.id)
      req.flash('info','Deleted')
      return res.redirect('/admin/abouts/values/')
    } catch (error) {
      res.status(500).json({ error })
    }

  }

}