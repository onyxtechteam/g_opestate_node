import ListingLocationModel from "../model/ListingLocation.js"
import { check, validationResult, matchedData } from 'express-validator'
import fs from 'fs';
import path from "path";

const removeItem = (array, item) => {
  return array.filter((ele) => ele != item);
}
export default class ListingLocationController {


  static async index(req, res) {
    try {
      const result = await ListingLocationModel.find().limit(5)
      return res.render('./admin/listing_location/index', { layout: './admin/layout', data: result })
    } catch (error) {
      res.status(500).json({ error })
    }

  }


  static async store(req, res) {
    try {

      const { location, summary, features, instagram, facebook, email, twitter, title, estate_gps } = req.body
      const socials = { facebook, email, twitter, instagram };
      const slug = title.toLowerCase()
        .replace(/[^\w ]+/g, '')
        .replace(/ +/g, '_');

      const last_entry = await ListingLocationModel.findOne().sort({ field: 'asc', _id: -1 }).limit(1)
      const count = last_entry?._id ? last_entry?._id + 1 : 1

      const body = { location, summary, title, socials, features, _id: count, slug, estate_gps: JSON.parse(estate_gps) }
      await ListingLocationModel.create(body);
      return res.json({ msg: 'Estate Added Added', type: 'success', })

    } catch (error) {
      console.log(error)
      res.json({ type: 'error', msg: 'Something went wrong' });
    }
  }

  static async imagesUpload(req, res) {
    try {

      let uploading_images = []
      const files = req.files
      files.forEach((file) => {
        uploading_images.push(file.filename)
      });
      const { images } = await ListingLocationModel.findById(req.params.id)
      uploading_images.push(...images)
      await ListingLocationModel.findByIdAndUpdate(req.params.id, { $set: { images: uploading_images } })
      req.flash('info', 'Image Uploaded')
      return res.redirect('/admin/listings/')

    } catch (error) {
      console.log(error)
      return res.render('./admin/listing_location/upload_image', { layout: './admin/layout', message: { type: 'error', msg: 'Something went wrong' }, data: result })
    }
  }

  static async videosUpload(req, res) {
    try {

      let uploading_videos = []
      const files = req.files
      files.forEach((file) => {
        uploading_videos.push(file.filename)
      });
      const { videos } = await ListingLocationModel.findById(req.params.id)
      uploading_videos.push(...videos)
      await ListingLocationModel.findByIdAndUpdate(req.params.id, { $set: { videos: uploading_videos } })
      req.flash('info', 'Video Uploaded')
      return res.redirect('/admin/listings/')

    } catch (error) {
      console.log(error)
      return res.render('./admin/listing_location/upload_image', { layout: './admin/layout', message: { type: 'error', msg: 'Something went wrong' }, data: result })
    }
  }

  static async videosDelete(req, res) {
    try {

      const { video } = req.query;
      const { videos } = await ListingLocationModel.findById(req.params.estate_id)
      const uploading_videos = removeItem(videos, video)
      await fs.unlinkSync(path.join("public/uploads/videos/", video))

      await ListingLocationModel.findByIdAndUpdate(req.params.estate_id, { $set: { videos: uploading_videos } })

      return res.json({ msg: 'Video Deleted', type: 'success', })

    } catch (error) {
      console.log(error)
      return res.json({ msg: 'Something went wrong', type: 'error', })
    }
  }

  static async imagesDelete(req, res) {
    try {

      const { image } = req.query;
      const { images } = await ListingLocationModel.findById(req.params.estate_id)
      const uploading_images = removeItem(images, image)
      await fs.unlinkSync(path.join("public/uploads/", image))

      await ListingLocationModel.findByIdAndUpdate(req.params.estate_id, { $set: { images: uploading_images } })

      return res.json({ msg: 'Image Deleted', type: 'success', })

    } catch (error) {
      console.log(error)
      return res.json({ msg: 'Something went wrong', type: 'error', })
    }
  }


  static async renderUploadImage(req, res) {
    try {

      const result = await ListingLocationModel.findById(req.params.id)
      return res.render('./admin/listing_location/upload_image', { layout: './admin/layout', message: {}, data: result })
    } catch (error) {
      res.status(500).json({ error })
    }
  }

  static async renderUploadVideo(req, res) {
    try {

      const result = await ListingLocationModel.findById(req.params.id)
      return res.render('./admin/listing_location/upload_video', { layout: './admin/layout', message: {}, data: result })
    } catch (error) {
      res.status(500).json({ error })
    }
  }


  static async edit(req, res) {
    try {

      const result = await ListingLocationModel.findById(req.params.id)
      let socials = {}
      for (const [key, value] of result?.socials) {
        socials[key] = value
      }
      const data = { ...result._doc, socials }
      return res.render('./admin/listing_location/edit', { layout: './admin/layout', message: {}, data })
    } catch (error) {
      console.log(error)
      res.status(500).json({ error })
    }

  }

  static async show(req, res) {
    try {

      const result = await ListingLocationModel.findById(req.params.id)
      let socials = {}
      for (const [key, value] of result?.socials) {
        socials[key] = value
      }
      const data = { ...result._doc, socials }
      return res.render('./admin/listing_location/show', { layout: './admin/layout', data })
    } catch (error) {
      console.log(error)
      res.status(500).json({ error })
    }
  }

  static async renderUploadLayout(req, res) {
    try {

      const result = await ListingLocationModel.findById(req.params.id)
      console.log(result)
      return res.render('./admin/listing_location/upload_layout', { layout: './admin/layout', data: result, message: {} })
    } catch (error) {
      console.log(error)
      res.status(500).json({ error })
    }

  }

  static async uploadLayout(req, res) {
    try {

      const { allocated, size } = req.body;
      const result = await ListingLocationModel.findById(req.params.id)
      const new_allocated = []
      new_allocated.push(...result?.allocated, { location: JSON.parse(allocated), size })
      await ListingLocationModel.findByIdAndUpdate(req.params.id, { $set: { allocated: new_allocated } })
      return res.render('./admin/listing_location/upload_layout', { layout: './admin/layout', data: result, message: { msg: 'Portion Allocated', type: 'success', } })

    } catch (error) {
      console.log(error)
      return res.json({ msg: 'Something went wrong', type: 'error', })
    }
  }

  static async fetchLayout(req, res) {
    try {
      const { allocated, estate_gps } = await ListingLocationModel.findById(req.params.id)
      return res.json({ data: { allocated, estate_gps }, type: 'success' })
    } catch (error) {
      console.log(error)
      return res.json({ msg: 'Something went wrong', type: 'error', })
    }
  }

  static async update(req, res) {
    try {
      const { location, summary, features, instagram, facebook, email, twitter, title, estate_gps } = req.body
      const socials = { facebook, email, twitter, instagram };
      const body = { location, summary, title, socials, features, estate_gps: JSON.parse(estate_gps) }
      await ListingLocationModel.findByIdAndUpdate(req.params.id, { $set: body })
      return res.json({ msg: 'Estate Info Updated', type: 'success' })
    } catch (error) {
      console.log(error)
      return res.json({ type: 'error', msg: 'Something went wrong' })
    }

  }

  static async delete(req, res) {
    try {

      const result = await ListingLocationModel.findByIdAndRemove(req.params.id)
      console.log(result)
      return res.redirect('/admin/listings/')
    } catch (error) {
      res.status(500).json({ error })
    }

  }

}