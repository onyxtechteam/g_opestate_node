import SliderModel from "../model/Slide.js"
import { check, validationResult, matchedData } from 'express-validator'
import FeaturedImagesModel from "../model/FeaturedImages.js"

export default class SliderController {

  static async index(req, res) {
    try {
      const result = await SliderModel.find().limit(5)
      const featured_images = await FeaturedImagesModel.find().count()
      return res.render('./admin/sliders/index', { layout: './admin/layout', slides: result,count:{featured_images} })
    } catch (error) {
      res.status(500).json({ error })
    }

  }


  static async store(req, res) {
    try {

      const { caption, no, title } = req.body
      const last_entry = await SliderModel.findOne().sort({field: 'asc', _id: -1 }).limit(1)
      const count = last_entry?._id ? last_entry?._id +1 : 1
      await SliderModel.create({ caption, no, _id: count , img_url: req.file.filename, title })
      res.render('./admin/sliders/create', { layout: './admin/layout', message: { type: 'success', msg: 'Slider Added' } })

    } catch (error) {
      console.log(error)
      res.render('./admin/sliders/create', { layout: './admin/layout', message: { type: 'error', msg: 'Something went wrong' } })
    }

  }

  static async edit(req, res) {
    try {

      const result = await SliderModel.findById(req.params.id)
      return res.render('./admin/sliders/edit', { layout: './admin/layout', message: { }, data: result })
    } catch (error) {
      res.status(500).json({ error })
    }

  }

  static async update(req, res,) {
    try {
      const { caption, title, no } = req.body

      const body = req.file?.filename ? {
        caption,
        title, no,
        img_url: req.file.filename,
      } : {
        caption,
        title, no,
      }

      const result = await SliderModel.findByIdAndUpdate(req.params.id, { $set: body })
      return res.redirect('/admin/sliders/')
    } catch (error) {
      const result = await SliderModel.findById(req.params.id)
      return res.render('./admin/sliders/edit', { layout: './admin/layout', message: { type: 'error', msg: 'Something went wrong' }, data: result })
    }

  }

  static async delete(req, res) {
    try {

      const result = await SliderModel.findByIdAndRemove(req.params.id)
      console.log(result)
      return res.redirect('/admin/sliders/')
    } catch (error) {
      res.status(500).json({ error })
    }

  }

}