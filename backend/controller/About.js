import AboutModel from "../model/About.js"
import { check, validationResult, matchedData } from 'express-validator'
import TestimonyModel from "../model/Testimony.js"
import TeamModel from "../model/Team.js"
import MissionModel from "../model/Mission.js"
import FaqModel from "../model/Faq.js"
import ValueModel from "../model/Value.js"

export default class AboutController {

  static async index(req, res) {
    try {
      const result = await AboutModel.find().limit(5)
      const testimonies = await TestimonyModel.find().count()
      const teams = await TeamModel.find().count()
      const missions = await MissionModel.find().count()
      const faqs = await FaqModel.find().count()
      const values = await ValueModel.find().count()
      return res.render('./admin/abouts/index', { layout: './admin/layout', data: result, count: { testimonies, teams, missions,faqs,values } })
    } catch (error) {
      console.log(error)
      return req.flash('error','Reload pLease')
    }
  }


  static async store(req, res) {
    try {
      const { about_us } = req.body
      const last_entry = await AboutModel.findOne().sort({ field: 'asc', _id: -1 }).limit(1)
      const count = last_entry?._id ? last_entry?._id + 1 : 1
      await AboutModel.create({ about_us: about_us, _id: count })
      res.render('./admin/abouts/create', { layout: './admin/layout', message: { type: 'success', msg: 'About Message Added' } })

    } catch (error) {
      console.log(error)
      res.render('./admin/abouts/create', { layout: './admin/layout', message: { type: 'error', msg: 'Something went wrong' } })
    }

  }

  static async edit(req, res) {
    try {

      const result = await AboutModel.findById(req.params.id)
      return res.render('./admin/abouts/edit', { layout: './admin/layout', message: { }, data: result })
    } catch (error) {
      res.status(500).json({ error })
    }

  }

  static async update(req, res,) {
    try {
      const { message } = req.body

      const body = {
        about_us: message
      }

      await AboutModel.findByIdAndUpdate(req.params.id, { $set: body })
      return res.redirect('/admin/abouts/')
    } catch (error) {
      const result = await AboutModel.findById(req.params.id)
      return res.render('./admin/abouts/edit', { layout: './admin/layout', message: { type: 'error', msg: 'Something went wrong' }, data: result })
    }

  }

  static async delete(req, res) {
    try {

      await AboutModel.findByIdAndRemove(req.params.id)
      return res.redirect('/admin/abouts/')
    } catch (error) {
      res.status(500).json({ error })
    }

  }

}