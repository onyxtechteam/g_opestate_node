import AdminModel from "../model/Admin.js"
import { check, validationResult, matchedData } from 'express-validator'
import { getHashedPassword, generateAuthToken } from "../services/Auth.js"


export default class AdminController {

  static async index(req, res) {
    try {
      const result = await AdminModel.find().limit(5)

      return res.render('./admin/admins/index', { layout: './admin/layout', data: result })
    } catch (error) {
      console.log(error)
      return req.flash('error', 'Reload pLease')
    }
  }


  static async store(req, res) {
    try {
      const { fullname, email, password, username, c_password } = req.body
      if (password.length > 8) {
        if (password === c_password) {
          if (await AdminModel.find().count() > 0) {
            const user = await AdminModel.findOne({email})
            if (user) {
              res.render('./admin/admins/create', { layout: './admin/layout', message: { type: 'error', msg: 'User Already Added' } })
              return;
            }
            const hashed = getHashedPassword(password)
            const last_entry = await AdminModel.findOne().sort({ field: 'asc', _id: -1 }).limit(1)
            const count = last_entry?._id ? last_entry?._id + 1 : 1
            await AdminModel.create({ fullname, email, password: hashed, username, _id: count })
            res.render('./admin/admins/create', { layout: './admin/layout', message: { type: 'success', msg: 'User Created' } })
          } else {
            const hashed = getHashedPassword(password)
            const last_entry = await AdminModel.findOne().sort({ field: 'asc', _id: -1 }).limit(1)
            const count = last_entry?._id ? last_entry?._id + 1 : 1
            await AdminModel.create({ fullname, email, password: hashed, username, _id: count })
            res.render('./admin/admins/create', { layout: './admin/layout', message: { type: 'success', msg: 'User Created' } })
          }

        } else {
          res.render('./admin/admins/create', { layout: './admin/layout', message: { type: 'error', msg: 'Password Mismatch' } })
        }
      } else {
        res.render('./admin/admins/create', { layout: './admin/layout', message: { type: 'error', msg: 'Password Too short' } })
      }
    } catch (error) {
      console.log(error)
      res.render('./admin/admins/create', { layout: './admin/layout', message: { type: 'error', msg: 'Something went wrong' } })
    }

  }


  static async login(req, res) {
    const defaultUser = {
      email: 'admin@gardenofpeaceestates.com',
      password: 'adminuser'
    }
    try {
      const { email, password } = req.body
      if (password.length > 8) {
        const hashedPassword = getHashedPassword(password);
        if (email === defaultUser.email && password === defaultUser.password) {
          const authToken = generateAuthToken();
          req.session.token = authToken
          res.redirect('/admin/')
        } else if (email != defaultUser.email && password != defaultUser.password) {
          const user = await AdminModel.findOne({ email: email, password: hashedPassword }).limit(1);
          if (user) {
            const authToken = generateAuthToken();
            req.session.token = authToken
            res.redirect('/admin/')
            return
          } else {
            res.render('./admin/login', { layout: false, message: { type: 'error', msg: 'Invalid Login' } })
            console.log({ type: 'error', msg: 'Invalid Login ' })
            return
          }
        } else {
          console.log({ type: 'error', msg: 'Invalid Login' })
          res.render('./admin/login', { layout: false, message: { type: 'error', msg: 'Invalid Login' } })
        }

      } else {
        res.render('./admin/login', { layout: false, message: { type: 'error', msg: 'Password Too Short' } })

      }
    } catch (error) {     
      console.log(error)
      res.render('./admin/login', { layout: false, message: { type: 'error', msg: 'Something went wrong' } })
    }

  }

  static async delete(req, res) {
    try {

      await AdminModel.findByIdAndRemove(req.params.id)
      return res.redirect('/admin/admins/')
    } catch (error) {
      res.status(500).json({ error })
    }

  }

  static async logout(req, res) {
    try {
      req.session.destroy((err) => {
        if (err) {
          return res.redirect('/admin/')
        }
        res.clearCookie('SESSION_NAME')
        return res.redirect('/login/')
      })
    } catch (error) {
      res.status(500).json({ error })
    }
  }

}