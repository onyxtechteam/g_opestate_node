import FaqModel from "../model/Faq.js"
import { check, validationResult, matchedData } from 'express-validator'

export default class FaqController {

  static async index(req, res) {
    try {
      const result = await FaqModel.find().limit(5)
      return res.render('./admin/faqs/index', { layout: './admin/layout', data: result })
    } catch (error) {
      res.status(500).json({ error })
    }
  }


  static async store(req, res) {
    try {
      const { question, answer } = req.body
      const last_entry = await FaqModel.findOne().sort({ field: 'asc', _id: -1 }).limit(1)
      const count = last_entry?._id ? last_entry?._id + 1 : 1
      await FaqModel.create({ question, answer, _id: count })
      res.render('./admin/faqs/create', { layout: './admin/layout', message: { type: 'success', msg: 'Faq Item Added' } })

    } catch (error) {
      res.render('./admin/faqs/create', { layout: './admin/layout', message: { type: 'error', msg: 'Something went wrong' } })
    }

  }

  static async edit(req, res) {
    try {

      const result = await FaqModel.findById(req.params.id)
      return res.render('./admin/faqs/edit', { layout: './admin/layout', message: { }, data: result })
    } catch (error) {
      res.status(500).json({ error })
    }

  }

  static async update(req, res,) {
    try {
      const { question, answer } = req.body
      console.log(answer)
      const body = {
        question, answer
      }

      await FaqModel.findByIdAndUpdate(req.params.id, { $set: body })
      return res.redirect('/admin/faqs/')
    } catch (error) {
      const result = await FaqModel.findById(req.params.id)
      return res.render('./admin/faqs/edit', { layout: './admin/layout', message: { type: 'error', msg: 'Something went wrong' }, data: result })
    }

  }

  static async delete(req, res) {
    try {

      await FaqModel.findByIdAndRemove(req.params.id)
      return res.redirect('/admin/faqs/')
    } catch (error) {
      res.status(500).json({ error })
    }

  }

}