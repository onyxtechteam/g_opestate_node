import ClientMessageModel from "../model/ClientMessage.js"
import { check, validationResult, matchedData } from 'express-validator'

export default class ClientMessageController {

  static async index(req, res) {
    try {
      const result = await ClientMessageModel.find().limit(5)

      return res.render('./admin/client_messages/index', { layout: './admin/layout', data: result })
    } catch (error) {
      console.log(error)
      return req.flash('error', 'Reload pLease')
    }
  }


  static async store(req, res) {
    try {
      const { name, message, email } = req.body
      console.log(req.body)
      const last_entry = await ClientMessageModel.findOne().sort({ field: 'asc', _id: -1 }).limit(1)
      const count = last_entry?._id ? last_entry?._id + 1 : 1
      await ClientMessageModel.create({ name, message, email, _id: count })

      return res.redirect('/')
    } catch (error) {
      console.log(error)
      req.flash('error', 'Something went wrong')
    }

  }


  static async delete(req, res) {
    try {

      await ClientMessageModel.findByIdAndRemove(req.params.id)
      return res.redirect('/admin/client_messages/')
    } catch (error) {
      res.status(500).json({ error })
    }

  }

}