import ServiceModel from "../model/Service.js"
import { check, validationResult, matchedData } from 'express-validator'

export default class ServiceController {

  static async index(req, res) {
    try {
      const result = await ServiceModel.find().limit(5)
      return res.render('./admin/services/index', { layout: './admin/layout', data: result })
    } catch (error) {
      res.status(500).json({ error })
    }
  }


  static async store(req, res) {
    try {

      const { summary, service } = req.body
      const last_entry = await ServiceModel.findOne().sort({ field: 'asc', _id: -1 }).limit(1)
      const count = last_entry?._id ? last_entry?._id + 1 : 1

      await ServiceModel.create({ summary, service, _id: count })
      res.render('./admin/services/create', { layout: './admin/layout', message: { type: 'success', msg: 'Service Added' } })

    } catch (error) {
      console.log(error)
      res.render('./admin/services/create', { layout: './admin/layout', message: { type: 'error', msg: 'Something went wrong' } })
    }

  }

  static async edit(req, res) {
    try {

      const result = await ServiceModel.findById(req.params.id)
      return res.render('./admin/services/edit', { layout: './admin/layout', message: { }, data: result })
    } catch (error) {
      res.status(500).json({ error })
    }

  }

  static async update(req, res, next) {
    try {
      const { summary, service } = req.body
      const body = { summary, service }

      await ServiceModel.findByIdAndUpdate(req.params.id, { $set: body })
      return res.redirect('/admin/services/')
    } catch (error) {
      console.log(error)
      const result = await ServiceModel.findById(req.params.id)
      return res.render('./admin/services/edit', { layout: './admin/layout', message: { type: 'error', msg: 'Something went wrong' }, data: result })
    }

  }

  static async delete(req, res) {
    try {

      const result = await ServiceModel.findByIdAndRemove(req.params.id)
      console.log(result)
      return res.redirect('/admin/services/')
    } catch (error) {
      res.status(500).json({ error })
    }

  }

}