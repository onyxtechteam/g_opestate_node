import TeamModel from "../model/Team.js"
import { check, validationResult, matchedData } from 'express-validator'

export default class TeamController {

  static async index(req, res) {
    try {
      const result = await TeamModel.find().limit(5)
      return res.render('./admin/teams/index', { layout: './admin/layout', teams: result })
    } catch (error) {
      res.status(500).json({ error })
    }

  }


  static async store(req, res) {
    try {

      const { fullname, summary, title } = req.body
      const last_entry = await TeamModel.findOne().sort({ field: 'asc', _id: -1 }).limit(1)
      const count = last_entry?._id ? last_entry?._id + 1 : 1

      await TeamModel.create({ fullname, summary, _id: count, avatar: req.file.filename, title })
      res.render('./admin/teams/create', { layout: './admin/layout', message: { type: 'success', msg: 'Team Added' } })

    } catch (error) {
      console.log(error)
      res.render('./admin/teams/create', { layout: './admin/layout', message: { type: 'error', msg: 'Something went wrong' } })
    }

  }

  static async edit(req, res) {
    try {

      const result = await TeamModel.findById(req.params.id)
      return res.render('./admin/teams/edit', { layout: './admin/layout', message: { }, data: result })
    } catch (error) {
      res.status(500).json({ error })
    }

  }

  static async update(req, res) {
    try {
      const { fullname, summary, title } = req.body
      const body = req?.file?.filename ? {
        fullname,
        title,
        avatar: req.file.filename,
        summary
      } : {
        fullname,
        title,
        summary
      }

      await TeamModel.findByIdAndUpdate(req.params.id, { $set: body })
      return res.redirect('/admin/teams/')
    } catch (error) {
      const result = await TestimonyModel.findById(req.params.id)
      return res.render('./admin/teams/edit', { layout: './admin/layout', message: { type: 'error', msg: 'Something went wrong' }, teams: result })
    }

  }

  static async delete(req, res) {
    try {

      const result = await TeamModel.findByIdAndRemove(req.params.id)
      console.log(result)
      return res.redirect('/admin/teams/')
    } catch (error) {
      res.status(500).json({ error })
    }

  }

}