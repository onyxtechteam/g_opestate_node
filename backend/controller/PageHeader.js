import { check, validationResult, matchedData } from 'express-validator'
import PageHeaderModel from '../model/PageHeader.js'

export default class PageHeaderController {

  static async index(req, res) {
    try {
      const result = await PageHeaderModel.find().limit(2)
      return res.render('./admin/page_headers/index', { layout: './admin/layout', data: result })
    } catch (error) {
      res.status(500).json({ error })
    }

  }


  static async store(req, res) {
    try {

      const { content,page } = req.body
      const last_entry = await PageHeaderModel.findOne().sort({ field: 'asc', _id: -1 }).limit(1)
      const count = last_entry?._id ? last_entry?._id + 1 : 1
      await PageHeaderModel.create({ content,page,_id: count})
      res.render('./admin/page_headers/create', { layout: './admin/layout', message: { type: 'success', msg: 'Page Header Added' } })

    } catch (error) {
      console.log(error)
      res.render('./admin/page_headers/create', { layout: './admin/layout', message: { type: 'error', msg: 'Something went wrong' } })
    }

  }

  static async edit(req, res) {
    try {

      const result = await PageHeaderModel.findById(req.params.id)
      return res.render('./admin/page_headers/edit', { layout: './admin/layout', message: { }, data: result })
    } catch (error) {
      res.status(500).json({ error })
    }

  }

  static async update(req, res, next) {
    try {
      const {content,page } = req.body

      const body = {content,page }

      await PageHeaderModel.findByIdAndUpdate(req.params.id, { $set: body })
      req.flash('info','Updated')
      return res.redirect('/admin/settings/page_headers/')
    } catch (error) {
      console.log(error)
      const result = await PageHeaderModel.findById(req.params.id)
      return res.render('./admin/page_headers/edit', { layout: './admin/layout', message: { type: 'error', msg: 'Something went wrong' }, data: result })
    }

  }

  static async delete(req, res) {
    try {

      const result = await PageHeaderModel.findByIdAndRemove(req.params.id)
      req.flash('info','Deleted')
      return res.redirect('/admin/settings/page_headers/')
    } catch (error) {
      res.status(500).json({ error })
    }

  }

}