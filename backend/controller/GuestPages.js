import AboutModel from "../model/About.js";
import FaqModel from "../model/Faq.js";
import FeaturedImagesModel from "../model/FeaturedImages.js";
import GeneralModel from "../model/General.js";
import ListingLocationModel from "../model/ListingLocation.js";
import MissionModel from "../model/Mission.js";
import PropertyModel from "../model/Property.js";
import ServiceModel from "../model/Service.js";
import SliderModel from "../model/Slide.js";
import TeamModel from "../model/Team.js";
import TestimonyModel from "../model/Testimony.js";
import ValueModel from "../model/Value.js";
export default class GuestPageController {

  static async homePage(req, res) {
    try {
      const teams = await TeamModel.find().sort({ field: 'asc', _id: -1 }).limit(4)
      const testimonies = await TestimonyModel.find().sort({ field: 'asc', _id: -1 })
      const mission = await MissionModel.findOne().sort({ field: 'asc', _id: -1 }).limit(1)
      const value = await ValueModel.findOne().sort({ field: 'asc', _id: -1 }).limit(1)
      const services = await ServiceModel.find().sort({ field: 'asc', _id: -1 }).limit(4)
      const slides = await SliderModel.find().limit(5)
      const aboutUs = await AboutModel.findOne().sort({ field: 'asc', _id: -1 }).limit(1)
      const listingLocation = await ListingLocationModel.find().sort({ field: 'asc', _id: -1 }).limit(4)
      const listings = await PropertyModel.find().sort({ field: 'asc', _id: -1 }).limit(4)
      const featured_images = await FeaturedImagesModel.find().sort({ field: 'asc', _id: -1 }).limit(4)
      const general = await GeneralModel.findOne().sort({ field: 'asc', _id: -1 })
      return res.render('./guest/index', { title: 'Home', slides, properties: [], mission, testimonies, services, teams, value, aboutUs, listings, listingLocation, featured_images, general })
    } catch (error) {
      return req.flash('error', 'Reload pLease')
    }
  }

  static async aboutPage(req, res) {
    try {
      const teams = await TeamModel.find().sort({ field: 'asc', _id: -1 }).limit(4)
      const aboutUs = await AboutModel.findOne().sort({ field: 'asc', _id: -1 }).limit(1)
      const mission = await MissionModel.findOne().sort({ field: 'asc', _id: -1 }).limit(1)
      const services = await ServiceModel.find().sort({ field: 'asc', _id: -1 }).limit(4)
      const slides = await SliderModel.find().limit(5)
      const listingLocation = await ListingLocationModel.find().sort({ field: 'asc', _id: -1 }).limit(4)
      const general = await GeneralModel.findOne().sort({ field: 'asc', _id: -1 })

      return res.render('./guest/about', { title: 'About Us', slides, mission, services, teams, aboutUs, listingLocation, general })
    } catch (error) {
      return req.flash('error', 'Reload pLease')
    }
  }

  static async faqPage(req, res) {
    try {
      const faqs = await FaqModel.find()
      const listingLocation = await ListingLocationModel.find().sort({ field: 'asc', _id: -1 }).limit(4)

      return res.render('./guest/faq', { title: 'FAQs', faqs, listingLocation })
    } catch (error) {
      return req.flash('error', 'Reload pLease')
    }
  }

  static async servicePage(req, res) {
    try {
      const faqs = await FaqModel.find().limit(3)
      const services = await ServiceModel.find().sort({ field: 'asc', _id: -1 })
      const mission = await MissionModel.findOne().sort({ field: 'asc', _id: -1 })
      const value = await ValueModel.findOne().sort({ field: 'asc', _id: -1 })
      const aboutUs = await AboutModel.findOne().sort({ field: 'asc', _id: -1 }).limit(1)
      const listingLocation = await ListingLocationModel.find().sort({ field: 'asc', _id: -1 })
      const featured_images = await FeaturedImagesModel.find().sort({ field: 'asc', _id: -1 }).limit(4)
      const listings = await PropertyModel.find().sort({ field: 'asc', _id: -1 })

      return res.render('./guest/services', { title: 'Services', faqs, services, mission, value, aboutUs, listingLocation, featured_images, listings })
    } catch (error) {
      return req.flash('error', 'Reload pLease')
    }
  }

  static async listingDetailsPage(req, res) {
    try {
      const listingLocation = await ListingLocationModel.find().sort({ field: 'asc', _id: -1 })
      const details = await PropertyModel.findOne({ _id: req.params.id }).limit(1)
      console.log(details)
      return res.render('./guest/project_details', { title: details.location, details, listingLocation, })
    } catch (error) {
      return req.flash('error', 'Reload pLease')
    }
  }


  static async listingPage(req, res) {
    try {
      const listings = await PropertyModel.find().sort({ field: 'asc', _id: -1 })
      const listingLocation = await ListingLocationModel.find().sort({ field: 'asc', _id: -1 })

      return res.render('./guest/properties', { title: 'Listings', listings, listingLocation, })
    } catch (error) {
      return req.flash('error', 'Reload pLease')
    }
  }

  static async contactUsPage(req, res) {
    try {
      const listingLocation = await ListingLocationModel.find().sort({ field: 'asc', _id: -1 })
      return res.render('./guest/contact_us', { title: 'Contact Us', listingLocation, message: {} })
    } catch (error) {
      return req.flash('error', 'Reload pLease')
    }
  }

  static async subEstateHome(req, res) {
    try {
      const listingLocation = await ListingLocationModel.find().sort({ field: 'asc', _id: -1 })
      const listings = await PropertyModel.find().sort({ field: 'asc', _id: -1 })
      const estate = await ListingLocationModel.findOne({ _id: req.params.id })
      return res.render('./guest/estate_home', { title: estate.title, listingLocation, listings, estate })
    } catch (error) {
      return req.flash('error', 'Reload pLease')
    }
  }

  static async subEstateMedia(req, res) {
    try {
      const listingLocation = await ListingLocationModel.find().sort({ field: 'asc', _id: -1 })
      const listings = await PropertyModel.find().sort({ field: 'asc', _id: -1 })
      const estate = await ListingLocationModel.findOne({ _id: req.params.id })
      let socials = {}
      for (const [key, value] of estate?.socials) {
        socials[key] = value
      }
      return res.render('./guest/estate_media', { title: estate.title, listingLocation, listings, estate, socials })
    } catch (error) {
      return req.flash('error', 'Reload pLease')
    }
  }

  static async error404(req, res) {
    try {
      const listingLocation = await ListingLocationModel.find().sort({ field: 'asc', _id: -1 })

      return res.render('./guest/not_found', { title: 'Page Not Found', listingLocation })
    } catch (error) {
      return req.flash('error', 'Reload pLease')
    }
  }
}