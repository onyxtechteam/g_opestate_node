import TestimonyModel from "../model/Testimony.js"
import { check, validationResult, matchedData } from 'express-validator'

export default class TestimonyController {

  static async index(req, res) {
    try {
      const result = await TestimonyModel.find().limit(5)
      return res.render('./admin/testimonies/index', { layout: './admin/layout', testimonies: result })
    } catch (error) {
      res.status(500).json({ error })
    }

  }


  static async store(req, res) {
    try {

      const { fullname, profession, testimony } = req.body
      const last_entry = await TestimonyModel.findOne().sort({ field: 'asc', _id: -1 }).limit(1)
      const count = last_entry?._id ? last_entry?._id + 1 : 1

      await TestimonyModel.create({ fullname, profession, testimony, _id: count, avatar: req.file.filename })
      res.render('./admin/testimonies/create', { layout: './admin/layout', message: { type: 'success', msg: 'Client Testimony Added' } })

    } catch (error) {
      console.log(error)
      res.render('./admin/testimonies/create', { layout: './admin/layout', message: { type: 'error', msg: 'Something went wrong' } })
    }

  }

  static async edit(req, res) {
    try {

      const result = await TestimonyModel.findById(req.params.id)
      return res.render('./admin/testimonies/edit', { layout: './admin/layout', message: { }, data: result })
    } catch (error) {
      res.status(500).json({ error })
    }

  }

  static async update(req, res, next) {
    try {
      const { fullname, profession, testimony } = req.body

      const body = req?.file?.filename ? {
        fullname, profession, testimony,
        avatar: req.file.filename,
      } :
        {
          fullname, profession, testimony,
        }
      await TestimonyModel.findByIdAndUpdate(req.params.id, { $set: body })
      return res.redirect('/admin/testimonies/')
    } catch (error) {
      console.log(error)
      const result = await TestimonyModel.findById(req.params.id)
      return res.render('./admin/testimonies/edit', { layout: './admin/layout', message: { type: 'error', msg: 'Something went wrong' }, data: result })
    }

  }

  static async delete(req, res) {
    try {

      const result = await TestimonyModel.findByIdAndRemove(req.params.id)
      console.log(result)
      return res.redirect('/admin/testimonies/')
    } catch (error) {
      res.status(500).json({ error })
    }

  }

}